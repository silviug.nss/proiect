import React, { useState, useEffect } from 'react';
import '../App.scss';
import axios from 'axios'
import {Link} from 'react-router-dom'

const Leaderboard = () => {

    const [users, setUsers] = useState([]);
    const [nrFlags, setNrFlags] = useState(0);

    useEffect(() => {
        // fetch questions
        axios.get('http://localhost:8000/quiz/users/flagged', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`,
            },
        }).then((response) => {
            console.log(response);
            setUsers(response.data.response);
        }).catch(error => console.error(error));
    }, []);

    const deleteUsers = () => {
        console.log(nrFlags);
        axios.post('http://localhost:8000/quiz/users/flagged', {nrFlags}, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`,
            },
        }).then((response) => {
            console.log(response);
        }).catch(error => console.error(error));
    }

    if (users.length !== 0) {
        return (
            <div>
                <table id="customers">
                    <thead>
                        <tr>
                            <th>{localStorage.getItem('ro') === 'true' ? "Pozitie" : "Position"}</th>
                            <th>ID</th>
                            <th>{localStorage.getItem('ro') === 'true' ? "Nume Utilizator" : "Username"}</th>
                            <th>{localStorage.getItem('ro') === 'true' ? "Scor" : "Score"}</th>
                            <th>{localStorage.getItem('ro') === 'true' ? "Scor Flags" : "Flags Score"}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            users.map((user, idx) => {
                                return (
                                    <tr key = {idx.toString()}>
                                        <td>{idx + 1}</td>
                                        <td>{user.id}</td>
                                        <td>{user.username}</td>
                                        <td>{user.score}</td>
                                        <td>{user.flags_score}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                <label>Nr. Flags <input type="number" id="nr_flags" max="0" value={nrFlags} onChange={(e)=>{setNrFlags(e.target.value)}}/> </label>
                <Link style={{textDecoration:"none"}} to='/quiz'>
                    <button className="submitUsers" onClick={(e) => {deleteUsers();}}>{localStorage.getItem('ro') === 'true' ? "Sterge Utilizatori" : "Delete Users"}</button>
                </Link>
            </div>
        );
    }
    return null;
}

export default Leaderboard