import React, { useState, useEffect } from 'react';
import '../App.scss';
import axios from 'axios'

const Leaderboard = () => {

    const [users, setUsers] = useState([]);

    useEffect(() => {
        // fetch questions
        axios.get('http://localhost:8000/quiz/users/best', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`,
            },
        }).then((response) => {
            console.log(response);
            setUsers(response.data.response);
        }).catch(error => console.error(error));
    }, []);

    if (users.length !== 0) {
        return (
            <table id="customers">
                <thead>
                    <tr>
                        <th>{localStorage.getItem('ro') === 'true' ? "Pozitie" : "Position"}</th>
                        <th>{localStorage.getItem('ro') === 'true' ? "Nume Utilizator" : "Username"}</th>
                        <th>{localStorage.getItem('ro') === 'true' ? "Scor Total" : "Total Score"}</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        users.map((user, idx) => {
                            return (
                                <tr key = {idx.toString()}>
                                    <td>{idx + 1}</td>
                                    <td>{user.username}</td>
                                    <td>{user.score}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        );
    }
    return null;
}

export default Leaderboard