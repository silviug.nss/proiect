import React, { useEffect, useState } from 'react';
import QuestionView from './QuestionView';
import axios from 'axios'
//import {Link} from 'react-router-dom'

const Flagged = () => {
    const [questions, setQuestions] = useState([]);
    //const [score, setScore] = useState(-1);

    useEffect(() => {
        // fetch questions
        axios.get('http://localhost:8000/quiz/questions/random', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`,
            },
        }).then((response)=> {console.log(response);
            setQuestions(response.data.response);
        }).catch(error => console.error(error));
    }, []);


    if (questions.length !== 0) {
        return(
            <div>
                <QuestionView question={questions[0]}/>
            </div>
        );
    }
    return null;
};

export default Flagged;