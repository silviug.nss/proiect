import React, { useState } from 'react';
import {Link} from 'react-router-dom'
import axios from 'axios'

const Autentificare = (props) => {
    //const [action, setAction] = useState('log')

    const action = props.action;
    const logHandler = props.logHandler;

    const [userInput, setUserInput] = useState({username:'', mail:'', password:''});
    const [checked, setChecked] = useState(localStorage.getItem('ro') === 'true');

    const handleAction = (type) => {
        if (userInput.username && userInput.password) {
            if(type==='log') {
                console.log('Autentificare log')
                axios.post('http://localhost:8000/quiz/users/login',
                {
                    username:userInput.username,
                    password:userInput.password,
                }).then((response) => {
                    console.log(response);
                    localStorage.setItem('jwt', response.data.response.token);
                    localStorage.setItem('role', response.data.response.role);
                    logHandler(localStorage.getItem('jwt') != null)
                })
                .catch(error => console.error(error))
            }
            else if (type==='reg') {
                if (userInput.mail) {
                    console.log('Autentificare reg')
                    axios.post('http://localhost:8000/quiz/users/register',
                    {
                        username:userInput.username,
                        mail:userInput.mail,
                        password:userInput.password,
                    }).then((response)=> {console.log(response)})
                    .catch(error => console.error(error))
                } else {
                    console.log('Esti prost! (reg)');
                }
            }
        } else {
            console.log('Esti prost! (log)');
        }
    }

    return(
        <form className='auth'>
            <div id='SwitchMes'>
                <div>{action === 'reg'?'Already have an account?':'Don\'t have an account?'}</div>
                {
                    action==='reg'
                    ?
                    <Link to='/login'>
                        <button className='button' >Login</button>
                    </Link>
                    :
                    <Link to='/register'>
                        <button className='button' >Register</button>
                    </Link>
                }
            </div>
                
            { action === 'reg' ? <h1>Register</h1> : <h1>Login</h1> }
            
            <div className='formitem'>
                <label>Username</label>
                <input type="text" onChange={(e)=>{let copy = JSON.parse(JSON.stringify(userInput)); copy.username=e.target.value; setUserInput(copy)}}/>
            </div>
            {
                action === 'reg' &&
                <div className='formitem'>
                    <label>Email</label>
                    <input type="text" onChange={(e)=>{let copy = JSON.parse(JSON.stringify(userInput)); copy.mail=e.target.value; setUserInput(copy)}}/>
                </div>
            }
            <div className='formitem'>
                <label>Password</label>
                <input type="password" onChange={(e)=>{let copy = JSON.parse(JSON.stringify(userInput)); copy.password=e.target.value; setUserInput(copy)}}/>
            </div>
            <button className='submit' onClick={(e)=>{e.preventDefault(); handleAction(action)}}>{action === 'reg'?'Register':'Login'}</button>
            {
                action === 'log' &&
                <label className="gameLogic">Prefer Romanian?
                    <input type="checkbox" checked={checked} onChange={(e) => {localStorage.setItem('ro', e.target.checked ? 'true' : 'false'); setChecked(e.target.checked)}}/>
                </label>
            }
        </form>
    )
}
export default Autentificare
