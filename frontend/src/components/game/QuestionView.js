import React from 'react';

const QuestionView = ({question, next}) => {
 
    return(
        <div className='questionView'>
            <p className='questionSt'>{question.statement}</p>
            <div className='variante'>
                <button className='box' onClick={(e) => {next('0')}}>{question.firstAnswer}</button>
                <button className='box' onClick={(e) => {next('1')}}>{question.secondAnswer}</button>
                <button className='box' onClick={(e) => {next('2')}}>{question.thirdAnswer}</button>
                <button className='box' onClick={(e) => {next('3')}}>{question.fourthAnswer}</button>
            </div>
        </div>
    )
};

export default QuestionView;