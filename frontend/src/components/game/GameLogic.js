import React, { useEffect, useState } from 'react';
import QuestionView from './QuestionView';
import axios from 'axios'
import {Link} from 'react-router-dom'

const GameLogic = () => {
    const [questions, setQuestions] = useState([]);
    const [qnr, setQnr] = useState(0);
    //const [score, setScore] = useState(-1);
    const [result, setResult] = useState(null);
    const [answers, setAnswers] = useState([]);
    const [checks, setChecks] = useState([]);

    useEffect(() => {
        // fetch questions
        axios.get('http://localhost:8000/quiz/questions/random', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`,
            },
        }).then((response)=> {console.log(response);
            setQuestions(response.data.response);
            setChecks(Array(response.data.response.length).fill(false));
        }).catch(error => console.error(error));
    }, []);

    const next = async (ans) => {
        setAnswers(answers.concat([{question_id: questions[qnr].id, answer : 'a' + ans}]));
        if (questions.length > qnr) {
            setQnr(qnr + 1);
            //if (qnr === questions.length -1) {
            //    await submit();
            //}
        }
    };

    const submit = async () => {
        // Send answer with axios
        console.log('Submit')
        axios.post('http://localhost:8000/quiz/questions/answer', {"answers": answers}, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('jwt')}`,
            },
        }).then((response)=> {console.log(response);
            setResult(response.data.response);
        }).catch(error => console.error(error));
    };

    const clickCheck = (idx) => () => {
        setChecks(checks.map((e, i) => i === idx? !e : e));
    };

    const sendFlags = () => {
        const questionIds = [];
        for (let i = 0; i < questions.length; i++) {
            if (checks[i])
                questionIds.push(questions[i].id);
        }

        if (questionIds.length > 0) {
            axios.post('http://localhost:8000/quiz/flagged_questions/flag', {"questionIds": questionIds}, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('jwt')}`,
                },
            }).then((response)=> {console.log(response);
            }).catch(error => console.error(error));
        }
    };

    if (result !== null) {
        //console.log('Score = ' + result.score);
        let msg = localStorage.getItem('ro') === 'true' ? `Ai raspuns la ${result.score} corecte! ` : `You answered ${result.score} correct!`;
        if (result.score < 3)
            msg += localStorage.getItem('ro') === 'true' ? 'Stim ca poti mai mult!' : 'We know you can do more!';
        else
            msg += localStorage.getItem('ro') === 'true' ? 'Tine-o tot asa!' : 'Keep it like this!';
        return(
            <div>
                <div> <p className="gameLogic">{msg}</p> </div>
                <div> <p className="gameLogic">{localStorage.getItem('ro') === 'true' ? 'Acum scorul tau total este' : 'Now your total score is'} {result.total_score}!</p></div>
                <div> <p className="gameLogic">{localStorage.getItem('ro') === 'true' ? 'Raspunsurile corecte erau:' : 'The correct answers were:'}</p></div>
                <table id="customers">
                    <thead>
                        <tr>
                            <th>Nr.</th>
                            <th>{localStorage.getItem('ro') === 'true' ? 'Raspunsul Tau' : 'Your Answer'}</th>
                            <th>{localStorage.getItem('ro') === 'true' ? 'Raspunsul Corect' : 'Correct Answer'}</th>
                            <th>Flag?</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            result.pairs.map((pair, idx) => {
                                return (
                                    <tr key = {idx.toString()}>
                                        <td>{idx + 1}</td>
                                        <td>{pair.chosen}</td>
                                        <td>{pair.real}</td>
                                        <td><input type="checkbox" onClick={clickCheck(idx)}/></td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                <Link style={{textDecoration:"none"}} to='/quiz'>
                    <button className="submitFlags" onClick={(e) => {console.log(checks); sendFlags();}}>{localStorage.getItem('ro') === 'true' ? "Trimite" : "Submit"}</button>
                </Link>
            </div>
        );
    }

    if (qnr === 5 && result === null) {
        submit();
        return null;
    }

    if (questions.length !== 0 && qnr !== 5) {
        return(
            <div>
                <QuestionView question={questions[qnr]} next={next}/>
            </div>
        );
    }
    return null;
};

export default GameLogic;