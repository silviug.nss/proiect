Microservice architecture quiz game developed with NodeJS, React and Docker.

Database, analytics & monitoring, gateway and pipelines were managed by Postgres & Adminer, Grafana & Prometheus, Kong and GitLab CI/CD.
