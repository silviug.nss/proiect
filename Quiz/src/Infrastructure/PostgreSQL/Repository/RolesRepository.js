const {
    queryAsync
} = require('..');

const addAsync = async (value) => {
    console.info(`Adding role ${value} in database async...`);

    const roles = await queryAsync('INSERT INTO roles (value) VALUES ($1) RETURNING *', [value]);

    return roles[0];
};

const getAllAsync = async() => {
    console.info(`Getting all roles from database async...`);

    return await queryAsync('SELECT * FROM roles');
};

const getByIdAsync = async(role_id) => {
    console.info(`Getting role with id=${role_id}from database async...`);

    return await queryAsync('SELECT * FROM roles where id = $1', [role_id]);
};

module.exports = {
    addAsync,
    getAllAsync,
    getByIdAsync
}