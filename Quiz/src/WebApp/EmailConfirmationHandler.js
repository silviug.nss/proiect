const express = require('express');
const { updateConfirmedMailAsync } = require('../Infrastructure/PostgreSQL/Repository/UsersRepository');
const jwt = require('jsonwebtoken');

const emailRouter = express.Router();

const EMAIL_SECRET = process.env.EMAIL_SECRET;

emailRouter.get('/:token', async(req, res) => {
    console.log("received this token", req.params.token);

    try {
        let userId = (jwt.verify(req.params.token, EMAIL_SECRET)).userId;  
        await updateConfirmedMailAsync(userId);
    } catch (e) {
        res.send('Error when updating your email confirmation status!');
    }
    res.send("Thank you for confirming your email!");
});

module.exports = emailRouter;
