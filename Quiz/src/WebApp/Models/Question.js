const ServerError = require('./ServerError.js');

class QuestionPostBody {
    constructor (body) {
        this.statement = body.full_statement;
        this.firstAnswer = body.a0;
        this.secondAnswer = body.a1;
        this.thirdAnswer = body.a2;
        this.fourthAnswer = body.a3;
        this.domain = body.domain;
        this.correct = body.correct;

        if (!this.statement || this.statement.length < 5) {
            throw new ServerError("The question is too short!", 400);
        }

        if (!this.firstAnswer) {
            throw new ServerError("The first (and right) answer is missing!", 400);
        }

        if (!this.secondAnswer) {
            throw new ServerError("The second answer is missing!", 400);
        }

        if (!this.thirdAnswer) {
            throw new ServerError("The third answer is missing!", 400);
        }

        if (!this.fourthAnswer) {
            throw new ServerError("The fourth answer is missing!", 400);
        }

        if (!this.correct) {
            throw new ServerError("The correct answer is missing!", 400);
        }
    }

    get Statement () {
        return this.statement;
    }

    get FirstAnswer () {
        return this.firstAnswer;
    }

    get SecondAnswer () {
        return this.secondAnswer;
    }

    get ThirdAnswer () {
        return this.thirdAnswer;
    }

    get FourthAnswer () {
        return this.fourthAnswer;
    }

    get Domain () {
        return this.domain;
    }

    get Correct () {
        return this.correct;
    }
}

class QuestionPutBody extends QuestionPostBody {
    constructor (body, id) {
        super(body);
        this.id = parseInt(id);

        if (!this.id || this.id < 1) {
            throw new ServerError("Id should be a positive integer", 400);
        }
    }

    get Id () {
        return this.id;
    }
}

class QuestionResponse {
    constructor(question) {
        this.id = question.id;
        this.statement = question.full_statement;
        this.firstAnswer = question.a0;
        this.secondAnswer = question.a1;
        this.thirdAnswer = question.a2;
        this.fourthAnswer = question.a3;
        this.domain = question.domain;
    }
}

class QuestionManagerResponse {
    constructor(question) {
        this.id = question.id;
        this.statement = question.full_statement;
        this.firstAnswer = question.a0;
        this.secondAnswer = question.a1;
        this.thirdAnswer = question.a2;
        this.fourthAnswer = question.a3;
        this.domain = question.domain;
        this.correct = question.correct;
    }
}

class QuestionAnswerBody {
    constructor (answers) {
        this.answers = answers;   
    }

    get Answers () {
        return this.answers;
    }
}

class QuestionAnswerResponseBody {
    constructor (body) {
        this.score = body.score;
        this.total_score = body.total_score;
    }
}

module.exports = {
    QuestionAnswerResponseBody,
    QuestionAnswerBody,
    QuestionManagerResponse,
    QuestionPostBody,
    QuestionPutBody,
    QuestionResponse,
}