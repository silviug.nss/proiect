const ServerError = require('./ServerError.js');

class UserLoginBody {
    constructor (body) {

        if (!body.username) {
            throw new ServerError("Username is missing", 400);
        }
    
        if (!body.password) {
            throw new ServerError("Password is missing", 400);
        }

        if (body.password.length < 4) {
            throw new ServerError("Password has less than 4 characters!", 400);
        }

        this.username = body.username;
        this.password = body.password;
    }

    get Username () {
        return this.username;
    }

    get Password () {
        return this.password;
    }
}

class UserRegisterBody extends UserLoginBody {
    constructor (body) {
        super(body);

        if (!body.mail) {
            throw new ServerError("Mail is missing!", 400);
        }

        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const isValid = re.test(String(body.mail).toLowerCase());

        if (!isValid) {
            throw new ServerError("Wrong format for mail!", 400);
        }

        this.mail = body.mail;
    }

    get Mail () {
        return this.mail;
    }
}

class UserRegisterResponse {
    constructor(user) {
        this.username = user.username;
        this.id = user.id;
        this.roleId = user.role_id;
    }
}

class UserScoreResponse {
    constructor(user) {
        this.username = user.username;
        this.score = user.score;
    }
}

class UserFlagsScoreResponse {
    constructor(user) {
        this.id = user.id;
        this.username = user.username;
        this.score = user.score;
        this.flags_score = user.flags_score;
    }
}

class UserLoginResponse {
    constructor(token, role) {
        this.role = role;
        this.token = token;
    }
}
module.exports =  {
    UserLoginBody,
    UserRegisterBody,
    UserLoginResponse,
    UserRegisterResponse,
    UserScoreResponse,
    UserFlagsScoreResponse
}