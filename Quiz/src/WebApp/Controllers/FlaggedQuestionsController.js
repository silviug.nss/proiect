const express = require('express');

const { getFirstNAsync, getByIdAsync, addByIdAsync, deleteByQidUidAsync, deleteByQidAsync } = require('../../Infrastructure/PostgreSQL/Repository/FlaggedQuestionsRepository.js');
const ServerError = require('../Models/ServerError.js');
const { FlaggedQuestionPostBody, FlaggedQuestionAddResponse, FlaggedQuestionGetResponse } = require('../Models/FlaggedQuestion.js');

const { setResponseDetails } = require('../Filters/ResponseFilter.js');

const { authorizeAndExtractTokenAsync } = require('../Filters/JWTFilter.js');
const AuthorizationFilter = require('../Filters/AuthorizationFilter.js');
const RoleConstants = require('../Constants/Roles.js');

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();


const Router = express.Router();

// DONE BY PLAYERS
Router.post('/flag/:questionId', authorizeAndExtractTokenAsync, async (req, res) => {
    const { questionId } = req.params;

    const flaggedQuestionBody = new FlaggedQuestionPostBody(questionId, req.user.userId);

    const flaggedQuestion = await addByIdAsync(flaggedQuestionBody.QuestionId, flaggedQuestionBody.UserId);

    if (!flaggedQuestion) {
        setResponseDetails(res, 404, `Question id not found!`);
    }

    setResponseDetails(res, 201, new FlaggedQuestionAddResponse(flaggedQuestion), req.originalUrl);
});

Router.post('/flag', jsonParser, authorizeAndExtractTokenAsync, async (req, res) => {
    const { questionIds } = req.body;
    const responses = [];
    console.log(questionIds);

    for (const questionId of questionIds) {
        const flaggedQuestionBody = new FlaggedQuestionPostBody(questionId, req.user.userId);

        const alreadyExists = await getByIdAsync(flaggedQuestionBody.QuestionId, flaggedQuestionBody.UserId);
        if (alreadyExists.length == 0) {
            const flaggedQuestion = await addByIdAsync(flaggedQuestionBody.QuestionId, flaggedQuestionBody.UserId);

            if (!flaggedQuestion) {
                setResponseDetails(res, 404, `Question id not found!`);
            }
            responses.push(new FlaggedQuestionAddResponse(flaggedQuestion));
        }
    }
    setResponseDetails(res, 201, responses, req.originalUrl);
});

// SIMILAR TO UNFLAG
Router.delete('/flag/:questionId', authorizeAndExtractTokenAsync, async (req, res) => {
    const { questionId } = req.params;

    const flaggedQuestion = await deleteByQidUidAsync(questionId, req.user.userId);

    setResponseDetails(res, 204, "Entity deleted succesfully");
});

// Done by MANAGERS
Router.get('/', authorizeAndExtractTokenAsync, AuthorizationFilter.authorizeRoles(RoleConstants.MANAGER), async (req, res) => {

    const flaggedQuestions = await getFirstNAsync(); 
    setResponseDetails(res, 200, flaggedQuestions.map(fq => new FlaggedQuestionGetResponse(fq)), req.originalUrl);
});


Router.delete('/:questionId', jsonParser, authorizeAndExtractTokenAsync, AuthorizationFilter.authorizeRoles(RoleConstants.MANAGER), async (req, res) => {
    const { questionId } = req.params;
    const msg_considered = req.body.msg_considered

    const flaggedQuestion = await deleteByQidAsync(questionId, msg_considered);

    setResponseDetails(res, 204, "FlaggedQuestion deleted succesfully");
});

module.exports = Router;