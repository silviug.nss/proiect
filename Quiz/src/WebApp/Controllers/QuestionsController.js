const express = require('express');

const { addAsync, addNoDomainAsync, getAllAsync, getRandomNByDomain, getRandomNAsync, getByIdAsync, updateByIdAsync, deleteByIdAsync, checkAnswers } = require('../../Infrastructure/PostgreSQL/Repository/QuestionsRepository.js');
const ServerError = require('../Models/ServerError.js');
const { QuestionPostBody, QuestionPutBody, QuestionResponse, QuestionManagerResponse, QuestionAnswerBody, QuestionAnswerResponseBody } = require('../Models/Question.js');
const { authorizeAndExtractTokenAsync } = require('../Filters/JWTFilter.js');
const { addScoreByIdAsync } = require('../../Infrastructure/PostgreSQL/Repository/UsersRepository.js');
const { setResponseDetails } = require('../Filters/ResponseFilter.js');
const AuthorizationFilter = require('../Filters/AuthorizationFilter.js');
const RoleConstants = require('../Constants/Roles.js');

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

const Router = express.Router();

Router.post('/', jsonParser, authorizeAndExtractTokenAsync, AuthorizationFilter.authorizeRoles(RoleConstants.MANAGER), async (req, res) => {
    
    const questionBody = new QuestionPostBody(req.body);

    if ('domain' in req.body)
        var question = await addAsync(questionBody.Statement, questionBody.FirstAnswer,
            questionBody.SecondAnswer, questionBody.ThirdAnswer, questionBody.FourthAnswer,
            questionBody.Domain, questionBody.Correct);
    else
        var question = await addNoDomainAsync(questionBody.Statement, questionBody.FirstAnswer,
            questionBody.SecondAnswer, questionBody.ThirdAnswer, questionBody.FourthAnswer,
            questionBody.Correct);
         
    setResponseDetails(res, 201, new QuestionManagerResponse(question), req.originalUrl);
});


Router.get('/all', authorizeAndExtractTokenAsync, AuthorizationFilter.authorizeRoles(RoleConstants.MANAGER), async (req, res) => {

    const questions = await getAllAsync();
    setResponseDetails(res, 200, questions.map(question => new QuestionManagerResponse({"id": question.question_id,
                        "full_statement": question.question_statement,
                        "a0": question.right_first_answer,
                        "a1": question.second_answer,
                        "a2": question.third_answer,
                        "a3": question.fourth_answer,
                        "domain": question.question_domain,
                        "correct": question.correct})));
});


Router.get('/random', authorizeAndExtractTokenAsync, async (req, res) => {

    const no_samples = 5
    const questions = await getRandomNAsync(no_samples);
    setResponseDetails(res, 200, questions.map(question => new QuestionResponse({"id": question.question_id,
                        "full_statement": question.question_statement,
                        "a0": question.right_first_answer,
                        "a1": question.second_answer,
                        "a2": question.third_answer,
                        "a3": question.fourth_answer,
                        "domain": question.question_domain})));
});


Router.get('/random/:domainName', authorizeAndExtractTokenAsync, async (req, res) => {

    const no_samples = 2
    let {
        domainName
    } = req.params;
    const questions = await getRandomNByDomain(no_samples, domainName);

    setResponseDetails(res, 200, questions.map(question => new QuestionResponse({"id": question.question_id,
                        "full_statement": question.question_statement,
                        "a0": question.right_first_answer,
                        "a1": question.second_answer,
                        "a2": question.third_answer,
                        "a3": question.fourth_answer,
                        "domain": domainName})));
});


Router.get('/:id', authorizeAndExtractTokenAsync, AuthorizationFilter.authorizeRoles(RoleConstants.MANAGER), async (req, res) => {
    let {
        id
    } = req.params;

    id = parseInt(id);

    if (!id || id < 1) {
        setResponseDetails(res, 400, "Id should be a positive integer");
    }
       
    const q = await getByIdAsync(id);
    
    if (q.length === 0) {
        setResponseDetails(res, 404, `Question with id ${id} does not exist!`);
    }
    let question = q[0]

    setResponseDetails(res, 200, new QuestionManagerResponse({"id": question.question_id,
                        "full_statement": question.question_statement,
                        "a0": question.right_first_answer,
                        "a1": question.second_answer,
                        "a2": question.third_answer,
                        "a3": question.fourth_answer,
                        "domain": question.question_domain,
                        "correct": question.correct}));
});


Router.put('/:id', jsonParser, authorizeAndExtractTokenAsync, AuthorizationFilter.authorizeRoles(RoleConstants.MANAGER), async (req, res) => {

    const questionBody = new QuestionPutBody(req.body, req.params.id);

    const question = await updateByIdAsync(questionBody.Id, questionBody.Statement, questionBody.FirstAnswer,
        questionBody.SecondAnswer, questionBody.ThirdAnswer, questionBody.FourthAnswer, questionBody.Domain,
        questionBody.Correct);
        
    if (question.length === 0) {
        setResponseDetails(res, 404, `Question with id ${id} does not exist!`);
    }
    console.log(question)
    setResponseDetails(res, 200, new QuestionManagerResponse({"id": question.id,
                        "full_statement": question.full_statement,
                        "a0": question.a0,
                        "a1": question.a1,
                        "a2": question.a2,
                        "a3": question.a3,
                        "domain": question.domain,
                        "correct": question.correct}));
});


Router.delete('/:id', authorizeAndExtractTokenAsync, AuthorizationFilter.authorizeRoles(RoleConstants.MANAGER), async (req, res) => {
    const { id } = req.params;

    if (!id || id < 1) {
        setResponseDetails(res, 404, "Id should be a positive integer");
    }
    
    const question = await deleteByIdAsync(parseInt(id));
    
    if (!question) {
        setResponseDetails(res, 404, `Question with id ${id} does not exist!`);
    }

    setResponseDetails(res, 204, "Entity deleted succesfully");
});

Router.post('/answer', jsonParser, authorizeAndExtractTokenAsync, async (req, res) => {
    const questionsBody = new QuestionAnswerBody(req.body.answers);
    
    const {score, pairs} = await checkAnswers(questionsBody.Answers);

    const total_score = await addScoreByIdAsync(req.user.userId, score);

    setResponseDetails(res, 200, {pairs: pairs, score: score, total_score: total_score}, req.originalUrl);
});


module.exports = Router;