const express = require('express');

const UsersManager = require('../../WebCore/Managers/UsersManager.js');
const UsersRepository = require('../../Infrastructure/PostgreSQL/Repository/UsersRepository.js');
const RolesRepository = require('../../Infrastructure/PostgreSQL/Repository/RolesRepository.js');
const { authorizeAndExtractTokenAsync } = require('../Filters/JWTFilter.js');
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();
const ServerError = require('../Models/ServerError.js');
const sendEmail = require('../EmailAuth');
const {
    UserLoginBody,
    UserRegisterBody,
    UserScoreResponse,
    UserRegisterResponse,
    UserLoginResponse,
    UserFlagsScoreResponse
} = require ('../Models/Users.js');

const ResponseFilter = require('../Filters/ResponseFilter.js');
const AuthorizationFilter = require('../Filters/AuthorizationFilter.js');
const RoleConstants = require('../Constants/Roles.js');

const Router = express.Router();

Router.post('/register', jsonParser, async (req, res) => {
    const userRegisterBody = new UserRegisterBody(req.body);
    console.log('reg body:', userRegisterBody);

    const user = await UsersManager.registerAsync(userRegisterBody.Username, userRegisterBody.Mail, userRegisterBody.Password);
    const reg_user = new UserRegisterResponse(user);
    await sendEmail(reg_user.id, userRegisterBody.Mail);

    ResponseFilter.setResponseDetails(res, 201, reg_user);
});

Router.post('/login', jsonParser, async (req, res) => {

    const userLoginBody = new UserLoginBody(req.body);
    console.log('log body:', userLoginBody);

    const userDto = await UsersManager.authenticateAsync(userLoginBody.Username, userLoginBody.Password);
    const user = new UserLoginResponse(userDto.Token, userDto.Role);

    ResponseFilter.setResponseDetails(res, 200, user);
});

Router.get('/', authorizeAndExtractTokenAsync, AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN), async (req, res) => {

    const users = await UsersRepository.getAllAsync();

    ResponseFilter.setResponseDetails(res, 200, users.map(user => new UserRegisterResponse(user)));
});


Router.get('/best', jsonParser, async (req, res) => {

    const users = await UsersRepository.getFirstNAsync();

    ResponseFilter.setResponseDetails(res, 200, users.map(user => new UserScoreResponse(user)));
});

Router.get('/flagged', jsonParser, async (req, res) => {

    const users = await UsersRepository.getFirstFlaggedNAsync();

    ResponseFilter.setResponseDetails(res, 200, users.map(user => new UserFlagsScoreResponse(user)));
});

Router.post('/flagged', jsonParser, authorizeAndExtractTokenAsync, AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN), async (req, res) => {

    const nrFlags = parseInt(req.body.nrFlags);
    console.log(req.body);

    if (!nrFlags)
        ResponseFilter.setResponseDetails(res, 400, `Numarul de flaguri nu este corect!`);

    const users = await UsersRepository.deleteByNrFlagsAsync(nrFlags);

    ResponseFilter.setResponseDetails(res, 200, users);
});

Router.put('/:userId/role/:roleId', jsonParser, authorizeAndExtractTokenAsync, AuthorizationFilter.authorizeRoles(RoleConstants.ADMIN), async (req, res) => {
    let {
        userId,
        roleId
    } = req.params;

    userId = parseInt(userId);
    roleId = parseInt(roleId);

    const user = await UsersRepository.getByUserIdAsync(userId);
    if (!user) {
        ResponseFilter.setResponseDetails(res, 404, `Nu exista utilizatorul cu id-ul ${userId}`);
    }
    const role = await RolesRepository.getByIdAsync(roleId);
    if (!role) {
        ResponseFilter.setResponseDetails(res, 404, `Nu exista rolul cu role_id-ul ${roleId}`);
    }
    const updatedUser = await UsersRepository.updateRoleByIdAsync(userId, roleId);
    ResponseFilter.setResponseDetails(res, 201, updatedUser);
});

module.exports = Router;